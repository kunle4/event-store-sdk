const EventStore = require('./event-store-service').eventStore;
const assert = require('assert');
const uuidv1 = require('uuid/v1');

const createEvent = (eventStore) => (eventName, eventChannel, data, parentId, cb) => {  
  assert(typeof cb === 'function', 'Ensure callback is present');
  assert(typeof eventName === 'string' && !!eventName , 'Ensure eventname is present');
  assert(typeof eventChannel === 'string' && !!eventChannel , 'Ensure eventChannel is present');
  assert(!!data , 'Ensure data is present');

  const eventId = uuidv1();

  const message = {
    eventId,
    parentId: parentId ? parentId : undefined,
    event: eventName,
    data
  };

  eventStore.add(eventChannel, message, cb);
  return eventId;
};


module.exports = {
  eventStore: (channelNames, connectionString) => EventStore(channelNames, connectionString),
  createEvent 
};
