const mongodb = require('mongodb');
const MongoDbQueue = require('mongodb-queue');
let eventStoredb;

const channelNames = ['USER','ACCOUNT'];

const EventStore = function(channelNames, connectionString){
    let channels = {};
    const _channelNames = channelNames;
    let self =this;
    
    mongodb.MongoClient.connect(connectionString, function(err, db) {
        eventStoredb = db;  
        //create channels   
        channelNames.forEach((channel) => {
            channels[channel] = MongoDbQueue(eventStoredb, channel);
        });
    });    

    const isChannelValid = (ch) => (typeof ch ==='string' && _channelNames.indexOf(ch) > -1);

    const getNotValidChannelErrorMessage = function(ch){
        let errMessage = `Invalid channel: ${ch} is not a valid channel, try one of ${_channelNames}`; 
    };

    const doAck = (msg, channel) => {
        channel.ack(msg.ack, (err, id) => {
            if(err){
                console.error(`Error while ack-ing msg ${msg.id} for event: ${msg.payload.event}`, err);
            }
            else{
                console.log(`Message popped msg off queue: ${id}, event : ${msg.payload.event}, tries: ${msg.tries}`)
            }
        });
    };

    const channelCallback = function (err) {
        const {msg, channel} = this;
        try{
            if(err) {
                if(msg.tries > 2) {
                    console.warn(`Forcing ack msg ${msg.id}, event : ${msg.payload.event}, tries: ${msg.tries}`)
                    doAck(msg, channel);
                } else {            
                    console.error(`EventStore#eventCallback: Error occured - on ${msg.tries} tries, will not ack msg for event ${msg.payload.event}`, err);
                }
                return;
            }      
            return doAck(msg, channel);  
        } catch (err) {
            console.error('Error occured, follow the clues', err);
        }
    };

    EventStore.prototype.addSecurly = function(channel,msg,callback){
        msg.isEncrypted = true;
        msg.data = securityService.encryptObject(msg.data);
        this.add(channel, msg, callback);
    };

    EventStore.prototype.add = function(channel,msg,callback){
        console.log('Adding to the event store', msg);
        if(isChannelValid(channel)){
            channels[channel].add(msg,callback);
        }else{
            callback(getNotValidChannelErrorMessage(channel),null);
        }
    };

    EventStore.prototype.get = function(channelName,callback){
        if(isChannelValid(channelName)){
            const channel = channels[channelName];            
            channel.get(function(err,msg){
                if(err){
                    console.error(err);
                    callback(err,null);
                }else{
                    if(!msg){
                        callback(null,null);
                    }
                    else{
                        const channelCallbackBoundToChannel = channelCallback.bind({channel, msg}); 
                        const response = {
                            msg: msg.payload,
                            callback: channelCallbackBoundToChannel
                        }
                        callback(null,response);                         
                    }

                }
            });
        }
        else{
            callback(getNotValidChannelErrorMessage(channelName),null);
        }
    }; 
}; 

module.exports = {
    eventStore: (channelNames, connectionString) => new EventStore(channelNames, connectionString),
    channelNames
};